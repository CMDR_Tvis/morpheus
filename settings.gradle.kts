rootProject.name = "morpheus"

pluginManagement {
    val kotlinVersion: String by settings
    plugins { kotlin("jvm") version kotlinVersion }
}
