# Morpheus [![Pipeline status](https://img.shields.io/gitlab/pipeline/CMDR_Tvis/morpheus?style=flat-square)](https://gitlab.com/CMDR_Tvis/morpheus/commits/master) [![License](https://img.shields.io/badge/license-MIT-44be16?style=flat-square)](LICENSE)

**Morpheus** is a simple sleep voting plugin with animated time skipping and fully configurable messages: when a 
share of players are in their beds, the night is skipped.

## Installing 

This plugin is designed for 1.15.2, older versions support is not guaranteed. To install Morpheus, you need to simply
put it into your `/plugins/` folder. Morpheus depends on 
[PluginApi](https://www.spigotmc.org/resources/pluginapi.71657/), you need to have it installed too.

## Commands

* `morpheus` &mdash; the root command;
    * `rl|reload` &mdash; reloads the configuration.

## Configuration

Morpheus can be configured using JSON configuration in `/plugins/Morpheus` directory.

Properties: 

* `object messages` &mdash; various messages sent or shown by the plugin: 
    * `object command` &mdash; messages that are sent as a result of command executing: 
        * `string noPermission` &mdash; sent when a player has no access to a certain subcommand,
        * `string configurationReloaded` &mdash; sent when a player reloads the configuration of the plugin,
        * `string unresolvedSubcommand` &mdash; sent when a player tries to call undefined subcommand;
    * `string leftBed` &mdash; sent when a player leaves his bed. Available placeholders: `%player%` &mdash; the name 
    of player, `%sleeping%` &mdash; percent of players in bed now; 
    * `object goodMorningTitle` &mdash; the title message sent when the night got skipped: 
        * `string title` &mdash; the title,
        * `string subtitle` &mdash; the subtitle,
        * `number fadeIn` &mdash; time of message fading in,
        * `number stay` &mdash; time the message stays,
        * `number fadeOut` &mdash; time of message fading out;
    * `string wentToBed` &mdash; sent when the last needed to skip the night player gets to bed. Available placeholder: 
    `%player%` the name of player; 
    * `string wentToBedNotEnough` &mdash; sent when a player gets to bed but there're not enough players to skip the 
    night. Available placeholders: `%player%` &mdash; the name of player, `%sleeping%` &mdash; percent of players in 
    bed now, `%notenough%` &mdash; the `notEnoughPlayersChoiceFormat` choice format with missing players quantity 
    passed; 
    * `string notEnoughPlayersChoiceFormat` &mdash; the choice format to handle plurality of missing players quantity, 
    this pattern can be written by `ChoiceFormat` type 
    [documentation](https://docs.oracle.com/javase/8/docs/api/java/text/ChoiceFormat.html); 
* `string notificationMode` &mdash; the way to notify the players about leaving and getting to bed, can be either 
`ACTION_BAR` or `CHAT`; 
* `boolean enableGoodMorningTitle` &mdash; if true, shows a title message to all the players when the night gets 
skipped by the plugin; 
* `number shareOfPlayers` &mdash; the decimal of players needed to skip the night. For example, 0.5 means a half of 
players, 0.9 means 90% of players. 

## Licensing

This project is licensed under the [MIT license](LICENSE).
