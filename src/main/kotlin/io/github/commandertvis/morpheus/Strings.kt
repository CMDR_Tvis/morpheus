package io.github.commandertvis.morpheus

import io.github.commandertvis.plugin.placeholder
import io.github.commandertvis.plugin.placeholders
import org.bukkit.entity.Player
import java.text.ChoiceFormat
import kotlin.math.roundToInt

fun String.replaceMorpheusPlaceholders(player: Player): String {
    val notEnough =(includedPlayers.count().toFloat() * shareOfPlayers).roundToInt() - sleepingPlayers.size

    return placeholders(
        "player" to player.displayName,
        "sleeping" to (currentRatio * 100).roundToInt(),

        "notenough" to ChoiceFormat(messages.notEnoughPlayersChoiceFormat)
            .format(notEnough)
            .placeholder("notenough" to notEnough)
    )
}
