package io.github.commandertvis.morpheus

import io.github.commandertvis.morpheus.configuration.Settings
import io.github.commandertvis.morpheus.handling.handlePlayerBedEnter
import io.github.commandertvis.morpheus.handling.handlePlayerBedLeave
import io.github.commandertvis.morpheus.handling.handlePlayerQuit
import io.github.commandertvis.plugin.command
import io.github.commandertvis.plugin.command.dsl.Contexts
import io.github.commandertvis.plugin.json.JsonConfigurablePlugin
import io.github.commandertvis.plugin.onlinePlayers
import io.github.commandertvis.plugin.register
import io.github.commandertvis.plugin.worlds
import org.bukkit.World
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.permissions.Permission
import org.bukkit.permissions.PermissionDefault
import java.util.*

lateinit var plugin: Morpheus
    private set

val sleepingPlayers: MutableSet<UUID> = hashSetOf()

lateinit var RELOAD_PERMISSION: Permission
    private set

val includedPlayers
    get() = onlinePlayers
        .asSequence()
        .filterNot(Player::isSleepingIgnored)
        .filter { worlds.indexOf(it.location.world) == 0 }

val currentRatio
    get() = sleepingPlayers.size.toDouble() / includedPlayers.count()

var isSkippingNow = false

class Morpheus : JsonConfigurablePlugin<Settings>(Settings()) {
    override fun onEnable() {
        plugin = this
        RELOAD_PERMISSION = Permission("${name.toLowerCase()}.reload", PermissionDefault.OP)
        saveDefaultConfig()
        registerPermission()
        registerCommand()
        registerListeners()
    }

    private fun registerPermission() = RELOAD_PERMISSION.register()

    private fun registerCommand() {
        command(name.toLowerCase()) {
            onCommandExecution { sender, _, _ ->
                if (!sender.hasPermission(RELOAD_PERMISSION)) {
                    sender.sendMessage(messages.command.noPermission)
                    cancel()
                }
            }

            default {
                action(@Contexts
                fun CommandSender.(_: String) = sendMessage(messages.command.unresolvedSubcommand))

                tabAction { _, _, _ -> emptyList() }
            }

            setOf("rl", "reload") subcommand {
                action(@Contexts
                fun CommandSender.(_: String) {
                    reloadConfig()
                    sendMessage(messages.command.configurationReloaded)
                })

                tabAction { _, _, _ -> emptyList() }
            }
        }
    }

    private fun registerListeners() {
        handlePlayerBedEnter()
        handlePlayerBedLeave()
        handlePlayerQuit()
    }
}
