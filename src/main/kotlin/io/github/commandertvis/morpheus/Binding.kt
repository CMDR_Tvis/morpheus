package io.github.commandertvis.morpheus

import io.github.commandertvis.morpheus.configuration.Settings
import io.github.commandertvis.plugin.json.binding

var enableGoodMorningTitle by plugin.binding(Settings::enableGoodMorningTitle)
var messages by plugin.binding(Settings::messages)
var notificationMode by plugin.binding(Settings::notificationMode)
var shareOfPlayers by plugin.binding(Settings::shareOfPlayers)
