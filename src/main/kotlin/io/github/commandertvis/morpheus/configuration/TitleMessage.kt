package io.github.commandertvis.morpheus.configuration

import org.bukkit.entity.Player

data class TitleMessage(val title: String, val subtitle: String, val fadeIn: Int, val stay: Int, val fadeOut: Int)

fun TitleMessage.send(player: Player) = player.sendTitle(title, subtitle, fadeIn, stay, fadeOut)
