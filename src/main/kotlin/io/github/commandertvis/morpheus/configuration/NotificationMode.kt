package io.github.commandertvis.morpheus.configuration

import net.md_5.bungee.api.ChatMessageType

enum class NotificationMode(val chatMessageType: ChatMessageType) {
    ACTION_BAR(ChatMessageType.ACTION_BAR),
    CHAT(ChatMessageType.CHAT);
}
