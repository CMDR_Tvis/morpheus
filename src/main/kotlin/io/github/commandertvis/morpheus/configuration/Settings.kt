package io.github.commandertvis.morpheus.configuration

class Settings {
    var messages = Messages()
    var notificationMode = NotificationMode.ACTION_BAR
    var enableGoodMorningTitle = true
    var shareOfPlayers = 0.5

    class Messages {
        var command = Command()
        var leftBed = "§f%player% §7got out of bed, %sleeping%% of players are sleeping."
        var goodMorningTitle = TitleMessage("§2Good morning,", "§2the night was skipped!", 20, 20, 20)
        var wentToBed = "§f%player% §7is now sleeping, skipping night."
        var wentToBedNotEnough = "§f%player% §7is now sleeping, %sleeping%% of player are sleeping. %notenough%."
        var notEnoughPlayersChoiceFormat = "1# §fOne §7player is not enough|1<§f%notenough% §7players are not enough"

        class Command {
            var noPermission = "§4No permission."
            var configurationReloaded = "§7Configuration reloaded."
            var unresolvedSubcommand = "§7Unknown subcommand. Available: reload."
        }
    }
}
