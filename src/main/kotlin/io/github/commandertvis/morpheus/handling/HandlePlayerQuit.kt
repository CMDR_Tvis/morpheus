package io.github.commandertvis.morpheus.handling

import io.github.commandertvis.morpheus.includedPlayers
import io.github.commandertvis.morpheus.sleepingPlayers
import io.github.commandertvis.plugin.handle
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.plugin.Plugin

fun Plugin.handlePlayerQuit() = handle<PlayerQuitEvent> {
    if (player in includedPlayers)
        sleepingPlayers -= player.uniqueId
}
