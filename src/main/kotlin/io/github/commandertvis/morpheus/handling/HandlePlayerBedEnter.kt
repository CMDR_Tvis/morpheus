package io.github.commandertvis.morpheus.handling

import io.github.commandertvis.morpheus.*
import io.github.commandertvis.morpheus.configuration.send
import io.github.commandertvis.plugin.*
import org.bukkit.Statistic
import org.bukkit.block.data.type.Bed
import org.bukkit.event.player.PlayerBedEnterEvent
import org.bukkit.plugin.Plugin

fun Plugin.handlePlayerBedEnter() = handleCancellable(ignoreCancelled = true, action = fun PlayerBedEnterEvent.() {
    if (bedEnterResult != PlayerBedEnterEvent.BedEnterResult.OK || player !in includedPlayers)
        return

    sleepingPlayers += player.uniqueId

    onlinePlayers.replyComponents(notificationMode.chatMessageType) {
        text {
            +(if (currentRatio >= shareOfPlayers)
                messages.wentToBed
            else
                messages.wentToBedNotEnough).replaceMorpheusPlaceholders(player)
        }
    }

    if (currentRatio >= shareOfPlayers) {
        skipNight()
    }
})

private fun skipNight() {
    isSkippingNow = true

    scheduler.runTaskTimer(
        plugin,
        { task ->
            if (worlds.first().time > 100)
                worlds.first().time += 40
            else {
                onlinePlayers.forEach {
                    it.setStatistic(Statistic.TIME_SINCE_REST, 0)

                    if (enableGoodMorningTitle && !it.isSleepingIgnored)
                        messages.goodMorningTitle.send(it)
                }

                isSkippingNow = false
                sleepingPlayers.clear()
                task.cancel()
            }
        },
        20L,
        1L,
    )
}
