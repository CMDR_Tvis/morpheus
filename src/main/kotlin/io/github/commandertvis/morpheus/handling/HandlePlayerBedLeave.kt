package io.github.commandertvis.morpheus.handling

import io.github.commandertvis.morpheus.*
import io.github.commandertvis.plugin.cancel
import io.github.commandertvis.plugin.handleCancellable
import io.github.commandertvis.plugin.onlinePlayers
import io.github.commandertvis.plugin.replyComponents
import org.bukkit.event.player.PlayerBedLeaveEvent
import org.bukkit.plugin.Plugin

fun Plugin.handlePlayerBedLeave() = handleCancellable(ignoreCancelled = true, action = fun PlayerBedLeaveEvent.() {
    if (player !in includedPlayers)
        return

    sleepingPlayers -= player.uniqueId

    if (isSkippingNow) {
        cancel()
        return
    }

    onlinePlayers.replyComponents(notificationMode.chatMessageType) {
        text { +messages.leftBed.replaceMorpheusPlaceholders(player) }
    }
})
