@file:Suppress("UNUSED_VARIABLE")

plugins { kotlin("jvm") }
val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val morpheusVersion: String by project
val pluginApiVersion: String by project
val spigotApiVersion: String by project
description = "A simple sleep voting plugin."
group = "io.github.commandertvis"
version = morpheusVersion

repositories {
    mavenCentral()
    maven("https://gitlab.com/api/v4/projects/10077943/packages/maven")
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots")
    maven("https://oss.sonatype.org/content/groups/public/")
}

dependencies {
    fun pluginApi(module: String) = "io.github.commandertvis.plugin:$module:$pluginApiVersion"
    compileOnly("org.spigotmc:spigot-api:$spigotApiVersion")
    compileOnly(pluginApi(module = "chat-components"))
    compileOnly(pluginApi(module = "command"))
    compileOnly(pluginApi(module = "common"))
}

tasks {
    compileKotlin.get().kotlinOptions {
        apiVersion = kotlinApiVersion
        jvmTarget = kotlinJvmTarget
        languageVersion = kotlinLanguageVersion
    }

    val copyToRun by creating(Copy::class) {
        from(jar)
        into("./run/plugins/")
    }

    processResources.get().expand("description" to description, "pluginName" to "Morpheus", "version" to version)
}
